#!/bin/bash

set -e

. .env

arg=$1

case "$arg" in
    "rebuild")
        docker-compose up -d --build reverse_proxy
        ;;
    "reload")
        docker exec $CONTAINER_NAME nginx -s reload
        ;;
    "restart")
        docker restart $CONTAINER_NAME
        ;;
    *)
        echo using \(\'rebuild\', \'restart\', \'reload\' as argument\)
esac

