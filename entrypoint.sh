#!/bin/bash


echo sdasdasd1 $0
echo dns_cloudflare_api_token = $CLOUDFLARE_API_TOKEN > cloudflare.ini
if [ "$1" == "certbot" ]
then
#    echo dns_cloudflare_api_token = $CLOUDFLARE_API_TOKEN >> cloudflare.ini
    if [[ -v CLOUDFLARE_API_TOKEN ]]
    then
	echo using dns cloudflare plugin for generate certificates
        certbot certonly --dns-cloudflare --dns-cloudflare-credentials /cloudflare.ini
    else
	echo using nginx plugin for generate certificates
        certbot certonly --nginx
    fi

#elif [ "$1" == "certbot_renew" ]
#then
#    echo dns_cloudflare_api_token = $CLOUDFLARE_API_TOKEN >> cloudflare.ini
#    echo "0 0 1 1-12/2 * /usr/local/bin/certbot renew --force-renewal >> /proc/1/fd/1 2>&1 " | crontab -
#    cron -f

elif [ "$1" == "nginx" ]
then
    if [ "$CERTBOT_RENEW" == "true" ]
    then
        echo "0 0 1 * * /usr/local/bin/certbot renew --force-renewal --dns-cloudflare-credentials /cloudflare.ini >> /proc/1/fd/1 2>&1 " | crontab -
        cron
    fi
    rm /etc/nginx/sites-enabled/*
    ln -s /etc/nginx/sites-available/*.conf /etc/nginx/sites-enabled/
    nginx -g  'daemon off;error_log /dev/stdout info;'
else
    exec $@
fi
