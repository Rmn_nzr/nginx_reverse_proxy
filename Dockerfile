FROM ubuntu:20.04

RUN apt update && apt install tzdata
RUN apt update && apt install -y python3 python3-dev python3-pip musl-dev libffi-dev nginx

RUN ln -sf /usr/share/zoneinfo/Asia/Tehran /etc/localtime

RUN pip3 install pip --upgrade
RUN pip3 install certbot-nginx certbot-dns-cloudflare

RUN apt-get install -y cron vim nginx logrotate
RUN mkdir /etc/letsencrypt/

RUN touch cloudflare.ini &&\
    chmod 600 cloudflare.ini
COPY entrypoint.sh /entrypoint.sh
COPY logrotate_nginx.conf /etc/logrotate.d/nginx
RUN chmod 644 /etc/logrotate.d/nginx
RUN chmod 755 /entrypoint.sh 

ENTRYPOINT ["/entrypoint.sh"]

